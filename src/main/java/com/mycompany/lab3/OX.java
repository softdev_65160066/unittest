/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author Chanon
 */
class OX {
    static boolean checkWin(String[][] Table,String currentPlayer){
        if (checkRow(Table,currentPlayer)|| checkCol(Table,currentPlayer)||checkDiagonaL(Table,currentPlayer)||checkDiagonalR(Table,currentPlayer)){
            return true;
        }
        else{
            /*
            checkDraw
 */
        }
        return false;
    }
    
    static boolean checkRow(String[][] Table,String currentPlayer){
        for(int row = 0;row<3;row++){
            if(checkRow(Table,currentPlayer,row)){
                return true;
            }
        }
        return false;
    }
    
    static boolean checkRow(String[][] Table,String currentPlayer,int row){
        return Table[row][0].equals(currentPlayer) && Table[row][1].equals(currentPlayer) && Table[row][2].equals(currentPlayer);
    }
    
      static boolean checkCol(String[][] Table,String currentPlayer){
        for(int col = 0;col<3;col++){
            if(checkCol(Table,currentPlayer,col)){
                return true;
            }
        }
        return false;
    }
      
     static boolean checkCol(String[][] Table,String currentPlayer,int col){
        return Table[0][col].equals(currentPlayer) && Table[1][col].equals(currentPlayer) && Table[2][col].equals(currentPlayer);
    }
     
     static boolean checkDiagonaL(String[][] Table,String currentPlayer){
        return Table[0][0].equals(currentPlayer) && Table[1][1].equals(currentPlayer) && Table[2][2].equals(currentPlayer);
    }
     
     static boolean checkDiagonalR(String[][] Table,String currentPlayer){
        return Table[0][2].equals(currentPlayer) && Table[1][1].equals(currentPlayer) && Table[2][0].equals(currentPlayer);
    } 
}
     

