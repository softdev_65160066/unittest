/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Chanon
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkWinRow1_O_output_true() {
    String [][] Table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinRow2_O_output_true() {
    String[][] Table = { {"-","-","-"},{"O","O","O"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinRow3_O_output_true() {
    String[][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinCol1_O_output_true() {
    String[][] Table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinCol2_O_output_true() {
    String[][] Table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }

    
    @Test
    public void checkWinCol3_O_output_true() {
    String[][] Table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinRow1_X_output_true() {
    String[][] Table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
   
    @Test
    public void checkWinRow2_X_output_true() {
    String[][] Table = {{"-","-","-"},{"X","X","X"},{"-","-","-"},};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinRow3_X_output_true() {
    String[][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinCol1_X_output_true() {
    String[][] Table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinCol2_X_output_true() {
    String[][] Table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }

    
    @Test
    public void checkWinCol3_X_output_true() {
    String[][] Table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinDiagonalL_O_output_true() {
    String[][] Table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinDiagonalR_O_output_true() {
    String[][] Table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinDiagonalL_X_output_true() {
    String[][] Table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinDiagonalR_X_output_true() {
    String[][] Table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(true,result);
    }
    
    @Test
    public void checkWinRowfalse1_1_O_output_false() {
    String [][] Table = {{"-","O","O"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse1_2_O_output_false() {
    String [][] Table = {{"O","-","O"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse1_3_O_output_false() {
    String [][] Table = {{"O","O","-"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_1_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","O","O"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_2_O_output_false() {
    String [][] Table = {{"-","-","-"},{"O","-","O"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_3_O_output_false() {
    String [][] Table = {{"-","-","-"},{"O","O","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse3_1_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"-","O","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }

    @Test
    public void checkWinRowfalse3_2_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"O","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
  
    @Test
    public void checkWinRowfalse3_3_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse1_1_X_output_false() {
    String [][] Table = {{"-","X","X"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse1_2_X_output_false() {
    String [][] Table = {{"X","-","X"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse1_3_X_output_false() {
    String [][] Table = {{"X","X","-"},{"-","-","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_1_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","X","X"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_2_X_output_false() {
    String [][] Table = {{"-","-","-"},{"X","-","X"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse2_3_X_output_false() {
    String [][] Table = {{"-","-","-"},{"X","X","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse3_1_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"-","X","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse3_2_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"X","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinRowfalse3_3_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse1_1_O_output_false() {
    String [][] Table = {{"-","-","-"},{"O","-","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse1_2_O_output_false() {
    String [][] Table = {{"O","-","-"},{"-","-","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }

    @Test
    public void checkWinColfalse1_3_O_output_false() {
    String [][] Table = {{"O","-","-"},{"O","-","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse2_1_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","O","-"},{"-","O","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse2_2_O_output_false() {
    String [][] Table = {{"-","O","-"},{"-","-","-"},{"-","O","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse2_3_O_output_false() {
    String [][] Table = {{"-","O","-"},{"-","O","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse3_1_O_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","O"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse3_2_O_output_false() {
    String [][] Table = {{"-","-","O"},{"-","-","-"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse3_3_O_output_false() {
    String [][] Table = {{"-","-","O"},{"-","-","O"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse1_1_X_output_false() {
    String [][] Table = {{"-","-","-"},{"X","-","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse1_2_X_output_false() {
    String [][] Table = {{"X","-","-"},{"-","-","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse1_3_X_output_false() {
    String [][] Table = {{"X","-","-"},{"X","-","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
  
    @Test
    public void checkWinColfalse2_1_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","X","-"},{"-","X","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    } 
    
    @Test
    public void checkWinColfalse2_2_X_output_false() {
    String [][] Table = {{"-","X","-"},{"-","-","-"},{"-","X","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }   
 
    @Test
    public void checkWinColfalse2_3_X_output_false() {
    String [][] Table = {{"-","X","-"},{"-","X","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }   
    
    @Test
    public void checkWinColfalse3_1_X_output_false() {
    String [][] Table = {{"-","-","-"},{"-","-","X"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse3_2_X_output_false() {
    String [][] Table = {{"-","-","X"},{"-","-","-"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinColfalse3_3_X_output_false() {
    String [][] Table = {{"-","-","X"},{"-","-","X"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL1_O_output_false() {
    String[][] Table = {{"-","-","-"},{"-","O","-"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL2_O_output_false() {
    String[][] Table = {{"O","-","-"},{"-","-","-"},{"-","-","O"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL3_O_output_false() {
    String[][] Table = {{"O","-","-"},{"-","O","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR1_O_output_false() {
    String[][] Table = {{"-","-","-"},{"-","O","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR2_O_output_false() {
    String[][] Table = {{"-","-","O"},{"-","-","-"},{"O","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR3_O_output_false() {
    String[][] Table = {{"-","-","O"},{"-","O","-"},{"-","-","-"}};
    String currentPlayer = "O";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL1_X_output_false() {
    String[][] Table = {{"-","-","-"},{"-","X","-"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL2_X_output_false() {
    String[][] Table = {{"X","-","-"},{"-","-","-"},{"-","-","X"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalL3_X_output_false() {
    String[][] Table = {{"X","-","-"},{"-","X","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR1_X_output_false() {
    String[][] Table = {{"-","-","-"},{"-","X","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR2_X_output_false() {
    String[][] Table = {{"-","-","X"},{"-","-","-"},{"X","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
    @Test
    public void checkWinDiagonalR3_X_output_false() {
    String[][] Table = {{"-","-","X"},{"-","X","-"},{"-","-","-"}};
    String currentPlayer = "X";
    boolean result = OX.checkWin(Table,currentPlayer);
    assertEquals(false,result);
    }
    
}
